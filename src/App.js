import React from 'react';
import {Route, Routes} from 'react-router-dom';
import WelcomeScreen from './views/pages/welcomeScreen/WelcomeScreen';
import {RouteName} from './constants';
import SignInScreen from './views/pages/signInScreen';
import SignUpScreen from './views/pages/signUpScreen';

const App = () => {
	return (
		<div>
			<Routes>
				<Route path={RouteName.WELCOME} element={<WelcomeScreen />} />
				<Route path={RouteName.SIGN_IN} element={<SignInScreen />} />
				<Route path={RouteName.SIGN_UP} element={<SignUpScreen />} />
			</Routes>
		</div>
	);

	// const adminUser = {
	//   email: "admin@admin.com",
	//   password: "admin123",
	// };

	// const [user, setUser] = useState({ name: "", email: "" });
	// const [error, setError] = useState("");

	// const Login = (details) => {
	//   console.log(details);
	//   if (
	//     details.email == adminUser.email &&
	//     details.password == adminUser.password
	//   ) {
	//     console.log("Logged in");
	//     setUser({
	//       name: details.name,
	//       email: details.email,
	//     });
	//   } else {
	//     setError("Details do not match");
	//     console.log("Details do not match");
	//   }
	// };

	// const Logout = () => {
	//   setUser({
	//     name: "",
	//     email: "",
	//   });
	// };

	// return (
	//   <div className="App">
	//     {user.email != "" ? (
	//       <div className="welcome">
	//         <h2>
	//           Welcome, <span>{user.name}</span>
	//         </h2>
	//         <button onClick={Logout}>Logout</button>
	//       </div>
	//     ) : (
	//       <LoginForm Login={Login} error={error}></LoginForm>
	//     )}
	//   </div>
	// );
};

export default App;
