import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const TextInputCT = ({label, type, onChange, value}) => {
	return (
		<div className="textInputCT">
			<label>{label}</label>
			<input type={type} onChange={onChange} value={value} />
		</div>
	);
};

export default TextInputCT;

TextInputCT.defaultProps = {
	label: 'Text',
	type: 'text',
	onChange: () => {},
	value: '',
};

TextInputCT.propTypes = {
	label: PropTypes.string,
	type: PropTypes.string,
	onChange: PropTypes.func,
	value: PropTypes.string,
};
