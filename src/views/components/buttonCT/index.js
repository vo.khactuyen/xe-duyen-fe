import React from 'react';
import PropTypes from 'prop-types';
import './style.css';

const ButtonCT = ({value, onSubmit}) => {
	return (
		<input
			className="buttonCT"
			type="submit"
			value={value}
			onSubmit={onSubmit}
		/>
	);
};

export default ButtonCT;

ButtonCT.defaultProps = {
	onSubmit: () => {},
	value: '',
};

ButtonCT.propTypes = {
	onSubmit: PropTypes.func,
	value: PropTypes.string,
};
