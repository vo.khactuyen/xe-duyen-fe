import React, {useState} from 'react';
import ButtonCT from '../../components/buttonCT';
import TextInputCT from '../../components/textInputCT';
import './style.css';

const SignInScreen = () => {
	const [credentials, setCredential] = useState({email: '', password: ''});
	
	const onSubmitSignIn = event => {
		event.preventDefault();
		console.log('submit');
	};
	
	return (
		<form className='signInScreen' onSubmit={onSubmitSignIn}>
			<h2>SIGN IN</h2>
			<TextInputCT
				label="Email"
				type="email"
				value={credentials.email}
				onChange={event => {
					setCredential({...credentials, email: event.target.value});
				}}
			/>
			<TextInputCT
				label="Password"
				type="password"
				value={credentials.password}
				onChange={event => {
					setCredential({...credentials, password: event.target.value});
				}}
			/>
			<ButtonCT value="SIGN IN" />
		</form>
	);
};

export default SignInScreen;
