import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {Navbar, Nav, NavDropdown} from 'react-bootstrap';
import './WelcomeScreen.css';
import logo from '../../../assets/app_logo.jpg';
import {RouteName} from '../../../constants';

const WelcomeScreen = () => {
	return (
		<div>
			<Navbar variant="light" fixed="top">
				<Navbar.Brand>
					<img src={logo} height="50" width="50" /> Xe Duyen
				</Navbar.Brand>
				<Nav>
					<Nav.Link href={RouteName.HOME}>Home</Nav.Link>
					<NavDropdown title="Blog">
						<NavDropdown.Item href={RouteName.BLOG_TRAVELLING}>Travelling</NavDropdown.Item>
						<NavDropdown.Item href={RouteName.BLOG_COOKING}>Cooking</NavDropdown.Item>
						<NavDropdown.Item href={RouteName.BLOG_MAKE_UP}>Make Up</NavDropdown.Item>
						<NavDropdown.Item href={RouteName.BLOG_GYM}>Gym</NavDropdown.Item>
						<NavDropdown.Divider />
						<NavDropdown.Item href={RouteName.BLOG_NETFLIX_AND_CHILL}>Netflix {'&'} Chill</NavDropdown.Item>
					</NavDropdown>
					<Nav.Link href={RouteName.SIGN_IN}>Sign In</Nav.Link>
					<Nav.Link bg="register" href={RouteName.SIGN_UP}>
						Register
					</Nav.Link>
				</Nav>
			</Navbar>
		</div>
	);
};

export default WelcomeScreen;
