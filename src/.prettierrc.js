module.exports = {
	arrowParens: "avoid",
	bracketSpacing: false,
	bracketSameLine: true,
	singleQuote: true,
	trailingComma: "all",
	tabWidth: 2,
	printWidth: 80,
	jsxSingleQuote: false
}