export const RouteName = {
	WELCOME: '/',
	SIGN_IN: 'sign-in',
	SIGN_UP: 'sign-up',
	SIGN_OUT: 'sign-out',
	HOME: 'home',
	BLOG: 'blog',
	BLOG_TRAVELLING: 'blog/travelling',
	BLOG_COOKING: 'blog/cooking',
	BLOG_MAKE_UP: 'blog/make-up',
	BLOG_GYM: 'blog/gym',
	BLOG_NETFLIX_AND_CHILL: 'blog/netflix-and-chill',
};
