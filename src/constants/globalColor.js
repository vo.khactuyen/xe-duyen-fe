export const GLOBAL_COLOR = {
	primaryColor: '#dc143c',
	backgroundColor: '#FEEFE3',
	textColor: '#1A1D32'
};